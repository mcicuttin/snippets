/* SILO is a scientific database library that can be found at the address
 *
 *    https://wci.llnl.gov/simulation/computer-codes/silo
 *
 * SILO works together with the scientific visualization code VisIt that
 * can be found at the address
 *
 *    https://wci.llnl.gov/simulation/computer-codes/visit
 *
 * Step 1: Installing SILO
 *
 * SILO is generally found in the package collection of your distribution.
 * On Debian for example you can install it via
 *
 *   apt install libsilo-dev libsiloh5-0
 *
 * If you are on OSX and use homebrew, try
 *
 *   brew tap datafl4sh/code
 *   brew install datafl4sh/code/silo
 *
 * and if it does not work, let me know at datafl4sh@toxicnet.eu or at
 * matteo.cicuttin@uliege.be.
 *
 * If you are on other Unices or on Windows, it is likely that you will need
 * to compile the library yourself. In that case:
 *
 * 1) Get the library from
     https://wci.llnl.gov/sites/wci/files/2021-01/silo-4.10.2-bsd.tgz
 * 2) tar -zxvf silo-4.10.2-bsd.tgz
 * 3) cd silo-4.10.2-bsd
 * 4) export SILO_PREFIX=<path where you want to install silo>
 * 5) ./configure --prefix=$SILO_PREFIX
 * 6) make && make install
 *
 * In order to compile your program and link against the copy of SILO you
 * have just configured and installed, do
 *
 * gcc -I$SILO_PREFIX/include -L$SILO_PREFIX/lib myprogram.c -lsilo
 *
 * If you installed SILO from your operating system package manager, there
 * should be no need to specify -I and -L paths, however there is a chance
 * that you will get the HDF5-enabled version. In that case, if -lsilo does
 * not work, try -lsiloh5.
 *
 * This program demonstrates SILO usage. Detailed information can be found in
 * https://wci.llnl.gov/sites/wci/files/2020-08/GettingDataIntoVisIt2.0.0.pdf.
 *
 * This program is compiled as described above. When run, it produces a
 * .silo file that you should be able to open in VisIt:
 *
 * 1) Launch VisIt
 * 2) Open your file using the "Open" function
 * 3) Add -> Mesh -> mesh
 * 4) Add -> Pseudocolor -> u
 *
 * You should now see the function x*sin(pi*y) plotted. Double-click on
 * "Pseudocolor - u" in the list of the variables and play with the plot
 * settings.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <silo.h>

#define DB_FILENAME "silo_test.silo"
#define DB_MESHNAME "mesh"

#define SIZE_X  16
#define SIZE_Y  32 

static void *
Malloc(size_t size)
{
    void *ret = malloc(size);
    if (!ret) {
        fprintf(stderr, "malloc() failed\n");
        exit(EXIT_FAILURE);
    }
    return ret;
}

int
main(int argc, const char *argv[])
{
    /* Open a SILO database, see pag. 17 listing 2-3 of the manual. */
    DBfile *db = DBCreate(DB_FILENAME, DB_CLOBBER, DB_LOCAL,
        "My first SILO test", DB_PDB);
    if (!db) {
        fprintf(stderr, "Could not create DB\n");
        return 1;
    }

    /* Write a cartesian mesh, see pag. 21 listing 2-9 of the manual.
     * If you want to write a 3D mesh, see listing 2-12. */
    double *x = (double *) Malloc(SIZE_X*sizeof(double));
    double *y = (double *) Malloc(SIZE_Y*sizeof(double));

    double dx = 1./(SIZE_X-1);
    for (size_t i = 0; i < SIZE_X; i++)
        x[i] = i*dx;

    double dy = 1./(SIZE_Y-1);
    for (size_t i = 0; i < SIZE_Y; i++)
        y[i] = i*dy;

    int dims[] = {SIZE_X, SIZE_Y};
    int ndims = 2;
    double *coords[] = {x, y};

    DBPutQuadmesh(db, DB_MESHNAME, NULL, coords, dims, ndims, DB_DOUBLE,
        DB_COLLINEAR, NULL);

    /* Write a nodal variable, see section 4.11 of the manual. */
    double *u = (double *) Malloc(SIZE_X*SIZE_Y*sizeof(double));

    for (size_t i = 0; i < SIZE_Y; i++)
        for (size_t j = 0; j < SIZE_X; j++)
            u[SIZE_X*i + j] = sin(i*dy*M_PI)*(j*dx);

    DBPutQuadvar1(db, "u", DB_MESHNAME, u, dims, ndims, NULL, 0, 
        DB_DOUBLE, DB_NODECENT, NULL);

    free(u);
    free(y);
    free(x);
    DBClose(db);
    return 0;
}
