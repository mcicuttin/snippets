set term postscript enhanced color
set output 'GTX760_cuda_vs_kokkos_float.eps'

set title '2D damped wave equation - float - GTX760'

set style data histogram
set style fill solid border -1
set logscale y
set grid ytics mytics

set arrow from 0,0.01 to 3,0.64 nohead lc rgb 'red'

plot 'GTX760_cuda_vs_kokkos_float.txt' using 2:xtic(1) ti col, '' u 3 ti col
