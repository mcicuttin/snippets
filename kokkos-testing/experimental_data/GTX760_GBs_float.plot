set term postscript enhanced color font ',18'
set output 'GTX760_GBs_float.eps'

set title "GTX760 float bandwidth (GB/s)"
set xlabel "Mesh size"
set ylabel "Measured bandwidth"
set logscale x
set yrange [40:150]

set label "NVidia declared: 192 GB/s" at 150,142
set label "Measured copy BW: 136 GB/s" at 150,138
set label "48.9% of copy BW" at 150,68
#set arrow 1 from 128,28.8 to 1024,28.8 nohead dt '.'
set arrow 2 from 128,136 to 1024,136 nohead dt '.'
set arrow 3 from 128,66.46 to 1024,66.46 nohead dt '.'
plot 'CUDA_GBs_float.txt' using 1:3:xtic(1) w lp lw 2 ti 'GTX760 GB/s'
