set term postscript enhanced color
set output 'AllTimes_double_update.eps'

set key left top

set title '2D damped wave equation - double'

set xlabel 'Mesh size'
set ylabel 'Iteration time [ms]'

set style data histogram
set style fill solid border -1
set logscale y
set grid ytics mytics

set arrow from 0,0.1 to 3,6.4 nohead lc rgb 'red'

plot 'AllTimes_double_update.txt' using 2:xtic(1) ti col, '' u 3 ti col, \
                        '' u 4 ti col, '' u 5 ti col, '' u 6 ti col
