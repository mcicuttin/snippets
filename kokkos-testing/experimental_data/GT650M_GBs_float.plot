set term postscript enhanced color font ',18'
set output 'GT650M_GBs_float.eps'

set title "GT650M float bandwidth (GB/s)"
set logscale x
set yrange [10:30]
set xlabel "Mesh size"
set ylabel "Measured bandwidth"

set label "NVidia declared: 28.8 GB/s" at 150,29.3
set label "Measured copy BW: 15.5 GB/s" at 150,16
set label "110.7% of copy BW" at 150,17.8
set arrow 1 from 128,28.8 to 1024,28.8 nohead dt '.'
set arrow 2 from 128,15.5 to 1024,15.5 nohead dt '.'
set arrow 3 from 128,17.16 to 1024,17.16 nohead dt '.'
plot 'CUDA_GBs_float.txt' using 1:2:xtic(1) w lp lw 2 ti 'GT650M GB/s'
