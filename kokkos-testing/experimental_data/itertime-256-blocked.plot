set term postscript enhanced color font ',18'
set output 'itertime-256-blocked.eps'

set logscale y
set grid xtics ytics mytics

set title 'Iteration time - SeqBlk'
set xlabel 'Iteration'
set ylabel 'Time [ms]'

plot 'itertime-256-blocked-float.txt' w l ti 'float', \
     'itertime-256-blocked-double.txt' w l ti 'double'
