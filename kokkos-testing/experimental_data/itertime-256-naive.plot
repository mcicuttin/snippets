set term postscript enhanced color font ',18'
set output 'itertime-256-naive.eps'

set logscale y
set grid xtics ytics mytics

set title 'Iteration time - Seq'
set xlabel 'Iteration'
set ylabel 'Time [ms]'

plot 'itertime-256-naive-float.txt' w l ti 'float', \
     'itertime-256-naive-double.txt' w l ti 'double'
