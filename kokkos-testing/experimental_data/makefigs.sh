#!/bin/sh

for file in *.plot; do
    gnuplot $file
done

for file in *.eps; do
    ps2pdf $file
done

for file in *.pdf; do
    pdfcrop $file
done

for file in *-crop.pdf; do
    mv $file $(echo $file | sed 's/-crop//')
done
