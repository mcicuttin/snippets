set term postscript enhanced color font ',20'
set output 'i7-3615QM_gflops_double_update.eps'

set title "i7-3615QM SeqBlk double"
set logscale x
set yrange [3:20]
set xlabel "Mesh size"
set ylabel "GFlops/s"

set label "Intel declared: 18.4 GFlops/s" at 150,18.8
set label "DGEMM: 3.83 GFlops/s" at 150,4.2
set arrow 1 from 128,18.4 to 1024,18.4 nohead dt '.'
set arrow 2 from 128,3.83 to 1024,3.83 nohead dt '.'
plot 'CUDA_gflops_double_update.txt' using 1:4:xtic(1) w lp lw 2 ti 'SeqBlk GFlops/s'
