set term postscript enhanced color font ',20'
set output 'GT650M_gflops_double.eps'

set title "GT650M double GFlops/s"
set logscale x
set yrange [12:22]
set xlabel "Mesh size"
set ylabel "GFlops/s"

set label "87.35% of hardware capacity" at 150,17.8
set arrow 1 from 128,20 to 1024,20 nohead dt '.'
set arrow 2 from 128,17.47 to 1024,17.47 nohead dt '.'
plot 'CUDA_gflops_double.txt' using 1:2:xtic(1) w lp lw 2 ti 'GT650M GFlops/s'
