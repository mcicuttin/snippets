set term postscript enhanced color font ',20'
set output 'GTX760_gflops_double.eps'

set title "GTX760 double GFlops/s"
set logscale x
set yrange [30:95]
set xlabel "Mesh size"
set ylabel "GFlops/s"

set label "76.3% of hardware capacity" at 150,70
set arrow 1 from 128,90 to 1024,90 nohead dt '.'
set arrow 2 from 128,68.64 to 1024,68.64 nohead dt '.'
plot 'CUDA_gflops_double.txt' using 1:3:xtic(1) w lp lw 2 ti 'GTX760 GFlops/s'
