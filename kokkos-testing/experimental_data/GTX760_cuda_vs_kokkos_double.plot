set term postscript enhanced color
set output 'GTX760_cuda_vs_kokkos_double.eps'

set title '2D damped wave equation - double - GTX760'

set style data histogram
set style fill solid border -1
set logscale y
set grid ytics mytics

set arrow from 0,0.02 to 3,1.28 nohead lc rgb 'red'

plot 'GTX760_cuda_vs_kokkos_double.txt' using 2:xtic(1) ti col, '' u 3 ti col
