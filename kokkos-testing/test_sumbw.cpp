#include <iostream>
#include <fstream>
#include <vector>
#include <chrono>
#include <numeric>

#include <cstring>

extern "C" {

void sum_restrict(const double * __restrict__ prev,
                  const double * __restrict__ curr,
                  double * __restrict__ next,
                  size_t sz)
{
    for (size_t i = 0; i < sz; i++)
        next[i] = curr[i] + prev[i];
}
}

template<typename T>
void
sum(const std::vector<T>& prev, const std::vector<T>& curr,
    std::vector<T>& next)
{
    if ( not (prev.size() == curr.size() and curr.size() == next.size()) )
        return;

    for (size_t i = 0; i < curr.size(); i++)
        next[i] = curr[i] + prev[i];
}

int main(void)
{
    using T = double;

    std::vector<std::vector<double>> itertimes;
    
    size_t maxiter = 500;

    for (size_t sz = 128; sz <= 8192; sz *= 2)
    {
        std::vector<T> prev(sz*sz);
        std::vector<T> curr(sz*sz);
        std::vector<T> next(sz*sz);

        for(size_t i = 0; i < sz*sz; i++)
        {
            prev[i] = i + 1;
            curr[i] = i % 18;
            next[i] = 37*i;
        }

        std::vector<double> itertime(maxiter);
        
        for (size_t iter = 0; iter < maxiter; iter++)
        {
            auto start = std::chrono::high_resolution_clock::now();
            //sum_restrict(prev.data(), curr.data(), next.data(), sz*sz);
            memcpy(next.data(), curr.data(), sz*sz*sizeof(T));
            std::swap(prev, curr);
            std::swap(curr, next);
            auto stop = std::chrono::high_resolution_clock::now();

            std::chrono::duration<double, std::milli> ms = stop - start;
            itertime[iter] = ms.count();
        }

        auto time = std::accumulate(itertime.begin(), itertime.end(), 0.0) / maxiter;
        std::cout << "Sum bandwidth: " <<  2*sizeof(T)*sz*sz/(1e6*time);
        std::cout << " GB/s"  << std::endl;

        itertimes.push_back( std::move(itertime) );
    }

    std::ofstream ofs("itertimes.txt");

    for (size_t i = 0; i < maxiter; i++)
    {
        for (size_t j = 0; j < itertimes.size(); j++)
        {
            ofs << itertimes[j][i] << " ";
        }
        ofs << std::endl;
    }
}



