#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cmath>
#include <chrono>
#include <type_traits>

#include <silo.h>
#include <Kokkos_Core.hpp>

template<typename T>
int
visit_dump(const Kokkos::View<T**>& kv, const std::string& fn)
{
    static_assert(std::is_same<T,double>::value or std::is_same<T,float>::value,
                  "Only double or float");

    DBfile *db = nullptr;
    db = DBCreate(fn.c_str(), DB_CLOBBER, DB_LOCAL, "Kokkos test", DB_HDF5);
    if (!db)
    {
        std::cout << "Cannot write simulation output" << std::endl;
        return -1;
    }

    auto size_x = kv.extent(0);
    auto size_y = kv.extent(1);
    
    std::vector<T> x(size_x);
    std::vector<T> y(size_y);
    
    for (size_t i = 0; i < size_x; i++)
        x.at(i) = T(i)/(size_x-1);
        
    for (size_t i = 0; i < size_y; i++)
        y.at(i) = T(i)/(size_y-1);
        
    int dims[] = { int(size_y), int(size_y) };
    int ndims = 2;
    T *coords[] = { x.data(), y.data() };
    
    if (std::is_same<T,float>::value)
        DBPutQuadmesh(db, "mesh", NULL, coords, dims, ndims,
                      DB_FLOAT, DB_COLLINEAR, NULL);

    if (std::is_same<T,double>::value)
        DBPutQuadmesh(db, "mesh", NULL, coords, dims, ndims,
                      DB_DOUBLE, DB_COLLINEAR, NULL);
    
    std::vector<T> data(x.size() * y.size());
    
    for (size_t i = 0; i < x.size(); i++)
        for (size_t j = 0; j < y.size(); j++)
            data.at(i*y.size()+j) = kv(i,j);
    
    if (std::is_same<T,float>::value)
        DBPutQuadvar1(db, "solution", "mesh", data.data(), dims, ndims,
                      NULL, 0, DB_FLOAT, DB_NODECENT, NULL);
    
    if (std::is_same<T,double>::value)
        DBPutQuadvar1(db, "solution", "mesh", data.data(), dims, ndims,
                      NULL, 0, DB_DOUBLE, DB_NODECENT, NULL);

    DBClose(db);
    return 0;
}

#define HIGH_ORDER

#ifdef HIGH_ORDER
    #define HALO_SIZE 4
#else
    #define HALO_SIZE 1
#endif

int main(int argc, char *argv[])
{
    using T = double;
    using namespace Kokkos;
    using namespace std::chrono;

    int     Nx          = 1024;
    int     Ny          = 1024;
    int     halo        = HALO_SIZE;

    int     rows        = Ny + 2*halo;
    int     cols        = Nx + 2*halo;
    int     timesteps   = 5000;
    T       dx          = 1./(Nx-1);
    T       dy          = 1./(Ny-1);
    T       dt          = 0.0001;

    T       velocity    = 1;
    T       damping     = 0.1;

    auto c2     = velocity*velocity;
    auto dx2    = dx*dx;
    auto dy2    = dy*dy;
    auto dt2    = dt*dt;

    auto D1 = 1./(1. + 0.5*damping*dt);
    auto D2 = 0.5*damping*dt - 1;

    Kokkos::initialize( argc, argv );

    {
        typedef View<T**> matrix_view_type;

        matrix_view_type u_prev("u_next", rows, cols);
        matrix_view_type u_curr("u_curr", rows, cols);
        matrix_view_type u_next("u_next", rows, cols);

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                if ( i < halo or i > rows-halo or j < halo or j > cols-halo)
                {
                    u_prev(i,j) = 0.0;
                    u_curr(i,j) = 0.0;
                }
                else
                {
                    T x = dx*i - 0.5;
                    T y = dy*j - 0.1;
                    u_prev(i,j) = -std::exp(-2400*(x*x + y*y));
                    u_curr(i,j) = 2*dt*u_prev(i,j);
                }
            }
        }

        MDRangePolicy<Rank<2>> range({halo,halo}, {rows-(halo+1), cols-(halo+1)});

#ifdef HIGH_ORDER
        static const T w0 = -205.0/72.0;
        static const T w1 =    8.0/5.0;
        static const T w2 =   -1.0/5.0;
        static const T w3 =    8.0/315.0;
        static const T w4 =   -1.0/560.0;
        static const T w[9] = { w4, w3, w2, w1, w0, w1, w2, w3, w4 };
#endif

        double iter_time = 0.0;
        for (int ts = 0; ts < timesteps; ts++)
        {
            auto t_begin = high_resolution_clock::now();

            parallel_for(range, KOKKOS_LAMBDA(int i, int j)
            {
#ifndef HIGH_ORDER
                u_next(i,j) = D1 * (
                            2.0*u_curr(i,j)
                            + D2*u_prev(i,j) +
                            + (c2*dt2/dy2) * 
                                (u_curr(i+1,j) - 2.0*u_curr(i,j) + u_curr(i-1,j))
                            + (c2*dt2/dx2) *
                                (u_curr(i,j+1) - 2.0*u_curr(i,j) + u_curr(i,j-1))
                            );

                if (j == 1)
                    u_next(i,j-1) = 0

                if (j == cols-2)
                    u_next(i,j+1) = 0;

                if (i == 1)
                    u_next(i-1,j) = 0;

                if (i == rows-2)
                    u_next(i+1,j) = 0;
#else
                /* 8th order in space */
                T deriv_x = 0.0;
                T deriv_y = 0.0;
                for (int k = -HALO_SIZE; k <= HALO_SIZE; k++)
                {
                    deriv_x += w[k+HALO_SIZE]*u_curr(i+k,j);
                    deriv_y += w[k+HALO_SIZE]*u_curr(i,j+k);
                }
                u_next(i,j) = D1*(2.0*u_curr(i,j) + D2*u_prev(i,j) +
                        + (c2*dt2/dy2)*(deriv_y)
                        + (c2*dt2/dx2)*(deriv_x));

                if (j == halo)
                    for (size_t k = 0; k < halo; k++)
                        u_next(i,k) = 0;

                if (j == cols-(halo+1))
                    for (size_t k = 0; k < halo; k++)
                        u_next(i,j+k+1) = 0;

                if (i == 1)
                    for (size_t k = 0; k < halo; k++)
                        u_next(k,j) = 0;

                if (i == rows-(halo+1))
                    for (size_t k = 0; k < halo; k++)
                        u_next(i+k+1,j) = 0;
#endif
            });
            
            std::swap(u_prev, u_curr);
            std::swap(u_curr, u_next);

            auto t_end = high_resolution_clock::now();

            duration<double> time_span =
                duration_cast<duration<double>>(t_end - t_begin);
            iter_time += time_span.count();

            
            if ( (ts % 100) == 0 )
            {
                std::stringstream ss;
                ss << "solution_" << ts << ".silo";
                visit_dump(u_curr, ss.str());
            }
            
        }

        double avg_iter_time = iter_time/timesteps;
        std::cout << "Average iteration time: " << avg_iter_time << std::endl;
    }

    Kokkos::finalize();

    return 0;
}

