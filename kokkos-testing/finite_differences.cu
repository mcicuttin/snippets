#include <iostream>
#include <fstream>
#include <vector>

#include <sstream>
#include <cstdio>


#include "fd_grid.hpp"
#include "fd_dataio.hpp"
#include "fd_wave_cpu.hpp"



int main(void)
{
#ifdef SINGLE_PRECISION
    using T = float;
#else
    using T = double;
#endif

    wave_equation_context<T> wec(512, 512, 1, 0.1, 0.0001, 5000);

#if 0
#ifdef __NVCC__
    wec.init();
    solve_cuda(wec);
#endif /* __NVCC__ */
#endif

    wec.init();
    solve_sequential(wec);

    wec.init();
    solve_sequential_blocked(wec);

    auto maxthreads = std::thread::hardware_concurrency();

    for (size_t i = 1; i <= maxthreads; i *= 2)
    {
        wec.init();
        solve_multithread(wec, i);
    }
}



