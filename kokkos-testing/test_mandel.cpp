#include <iostream>
#include <fstream>
#include <vector>
#include <chrono>

#include <pmmintrin.h>
#include <xmmintrin.h>

#define YMIN (-1.5)
#define YMAX (1.5)
#define XMIN (-2.5)
#define XMAX (1.5)

#define SIZE_Y	2560
#define SIZE_X  2048
#define GPU_BLOCK_X_SZ  32
#define GPU_BLOCK_Y_SZ  32

#define SY_TO_PLANE(y) (YMIN + y*(YMAX-YMIN)/SIZE_Y)
#define SX_TO_PLANE(x) (XMIN + x*(XMAX-XMIN)/SIZE_X)

#ifdef __NVCC__

template<typename T>
__global__
void gpu_mandel(T *mandel, int maxiter)
{
    int j = blockDim.x * blockIdx.x + threadIdx.x;
    int i = blockDim.y * blockIdx.y + threadIdx.y;

    if (j >= SIZE_X or i >= SIZE_Y)
        return;

	T z_re = 0;
	T z_im = 0;
	T c_re = SX_TO_PLANE(j);                    // 4 ops
	T c_im = SY_TO_PLANE(i);                    // 4 ops

			
	T zabs2 = (z_re * z_re) + (z_im * z_im);    // 3 ops
	int iter = 0;
                                                // = 11 ops always
	while (zabs2 < 4.0 && iter < maxiter)
	{
		T z_re2 = z_re*z_re;                    // 1 op
		T z_im2 = z_im*z_im;                    // 1 op
		T z_ri = 2*z_re*z_im;                   // 2 ops

		z_re = z_re2 - z_im2 + c_re;            // 2 ops
		z_im = z_ri + c_im;                     // 1 op

		zabs2 = z_re2 + z_im2;                  // 1 op
		iter++;
	}
                                                // = 8 ops * iter
	if (iter == maxiter)
		mandel[i*SIZE_X + j] = 0.0;
	else
		mandel[i*SIZE_X + j] = iter;
}

template<typename T>
void compute_gpu(std::vector<T>& mandel, int maxiter)
{
    dim3 grid_size(SIZE_X/GPU_BLOCK_SIZE_X + 1, SIZE_Y/GPU_BLOCK_SIZE_Y + 1);
    dim3 threads_per_block(WAVE_8_KER_COLS, WAVE_8_KER_ROWS);
}

#endif /* __NVCC__  */

int main(void)
{
	size_t maxiter = 1000;
	using T = float;

	std::vector<T> mandel(SIZE_X*SIZE_Y);

	auto start = std::chrono::high_resolution_clock::now();

    size_t flops = 0;

    //_MM_SET_FLUSH_ZERO_MODE(_MM_FLUSH_ZERO_ON);
    //_MM_SET_DENORMALS_ZERO_MODE(_MM_DENORMALS_ZERO_ON);

	for (size_t i = 0; i < SIZE_Y; i++)
	{
		for (size_t j = 0; j < SIZE_X; j++)
		{
			T z_re = 0;
			T z_im = 0;
			T c_re = SX_TO_PLANE(j);                    // 4 ops
			T c_im = SY_TO_PLANE(i);                    // 4 ops

			
			T zabs2 = (z_re * z_re) + (z_im * z_im);    // 3 ops
			size_t iter = 0;
                                                        // = 11 ops always
			while (zabs2 < 4.0 && iter < maxiter)
			{
				T z_re2 = z_re*z_re;                    // 1 op
				T z_im2 = z_im*z_im;                    // 1 op
				T z_ri = 2*z_re*z_im;                   // 2 ops

				z_re = z_re2 - z_im2 + c_re;            // 2 ops
				z_im = z_ri + c_im;                     // 1 op

				zabs2 = z_re2 + z_im2;                  // 1 op
				iter++;
			}
                                                        // = 8 ops * iter
			if (iter == maxiter)
				mandel[i*SIZE_X + j] = 0.0;
			else
				mandel[i*SIZE_X + j] = iter;
		}
	}

	for (size_t i = 0; i < SIZE_Y; i++)
		for (size_t j = 0; j < SIZE_X; j++)
            if (size_t ni = mandel[i*SIZE_X+j]; ni != 0)
                flops += 11+8*ni;
            else
                flops += 11+8*maxiter;

	auto stop = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> s = stop - start;

    std::cout << s.count() << std::endl;
    std::cout << flops << std::endl;

    std::cout << "GFlops/s: " << double(flops)/(1e6*s.count()) << std::endl;
    
	std::ofstream ofs("mandelbrot.txt");
	for (size_t i = 0; i < SIZE_Y; i++)
		for (size_t j = 0; j < SIZE_X; j++)
			ofs << j << " " << i << " " << mandel[i*SIZE_X + j] << std::endl;
	

	return 0;
}
