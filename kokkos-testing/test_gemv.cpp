#include <iostream>
#include <vector>
#include <chrono>

template<typename T>
void gemv(T alpha, const T *A, const T *x, T beta, T *y, size_t M, size_t N)
{
    for (size_t i = 0; i < M; i++)
        for (size_t j = 0; j < N; j++)
            y[i] = alpha*A[i*N+j]*x[j] + beta*y[i];
}

int main(void)
{
    using T = double;

    static const size_t loops = 10000;
    static const size_t M = 1000;
    static const size_t N = 1000;

    std::vector<T> A(M*N);
    std::vector<T> y(M), x(N);

    T alpha = 0.1;
    T beta = 0.1;

    auto start = std::chrono::high_resolution_clock::now();
    for (size_t i = 0; i < loops; i++)
        gemv(alpha, A.data(), x.data(), beta, y.data(), M, N);
    auto stop = std::chrono::high_resolution_clock::now();
    
    std::chrono::duration<double, std::milli> ms = stop - start;

    std::cout << "GEMV time: " << ms.count()/loops << " ms"  << std::endl;   

    return 0;
}
