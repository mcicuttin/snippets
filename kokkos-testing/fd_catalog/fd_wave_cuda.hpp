/* Performance evaluation of different implementations of finite difference
 * schemes.
 *
 * Matteo Cicuttin (C) 2020, Université de Liège, ACE Group
 */

#pragma once

double solve_cuda(wave_equation_context<float>&);
double solve_cuda(wave_equation_context<double>&);