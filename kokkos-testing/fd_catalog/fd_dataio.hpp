/* Performance evaluation of different implementations of finite difference
 * schemes.
 *
 * Matteo Cicuttin (C) 2020, Université de Liège, ACE Group
 */

#pragma once

#include "fd_grid.hpp"

#ifdef HAVE_SILO
#include <silo.h>
#endif /* HAVE_SILO */

#ifdef HAVE_SILO
template<typename T>
int
visit_dump(const fd_grid<T>& g, const std::string& fn)
{
    static_assert(std::is_same<T,double>::value or std::is_same<T,float>::value, "Only double or float");

    DBfile *db = nullptr;
    db = DBCreate(fn.c_str(), DB_CLOBBER, DB_LOCAL, "visit output", DB_PDB);
    if (!db)
    {
        std::cout << "Cannot write simulation output" << std::endl;
        return -1;
    }

    auto size_x = g.grid_cols();
    auto size_y = g.grid_rows();
    
    std::vector<T> x(size_x);
    std::vector<T> y(size_y);
    
    auto dx = g.dx();
    auto px = -dx * g.halo();
    for (size_t i = 0; i < size_x; i++)
        x.at(i) = px + i*dx;
        
    auto dy = g.dy();
    auto py = -dy * g.halo();
    for (size_t i = 0; i < size_y; i++)
        y.at(i) = py + i*dy;
        
    int dims[] = { int(size_x), int(size_y) };
    int ndims = 2;
    
    T *coords[] = {x.data(), y.data()};
    
    if (std::is_same<T,float>::value)
    {
        DBPutQuadmesh(db, "mesh", NULL, coords, dims, ndims, DB_FLOAT, DB_COLLINEAR, NULL);    
        DBPutQuadvar1(db, "solution", "mesh", g.data(), dims, ndims, NULL, 0, DB_FLOAT, DB_NODECENT, NULL);
    }
    else
    {
        DBPutQuadmesh(db, "mesh", NULL, coords, dims, ndims, DB_DOUBLE, DB_COLLINEAR, NULL);    
        DBPutQuadvar1(db, "solution", "mesh", g.data(), dims, ndims, NULL, 0, DB_DOUBLE, DB_NODECENT, NULL);
    }

    DBClose(db);
    return 0;
}
#endif /* HAVE_SILO */

