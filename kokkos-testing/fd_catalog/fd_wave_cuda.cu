/* Performance evaluation of different implementations of finite difference
 * schemes.
 *
 * Matteo Cicuttin (C) 2020, Université de Liège, ACE Group
 */

#include <iostream>
#include <cassert>
#include <sstream>

#include "fd_grid.hpp"
#include "fd_dataio.hpp"
#include "fd_wave.hpp"

#define DEBUG

#include "cuda_runtime.h"

inline cudaError_t checkCuda(cudaError_t result)
{
#if defined(DEBUG) || defined(_DEBUG)
  if (result != cudaSuccess) {
    fprintf(stderr, "CUDA Runtime Error: %s\n", cudaGetErrorString(result));
    assert(result == cudaSuccess);
  }
#endif
  return result;
}

/* 8th order 2nd derivative weights */
__constant__ float  d_w32[9];
__constant__ double d_w64[9];

#define WAVE_8_KER_ROWS           32
#define WAVE_8_KER_COLS           32

template<typename T>
__global__ void
wave_2D_kernel_cuda(T* u_prev, T* u_curr, T* u_next, wave_2D_params<T> params)
{
    /* This kernel saturates the memory bandwidth on GTX650M. Memory bw
     * was estimated using Mark Harris' copy/transpose code */

    //     HHHH
    //    H****H     H = halo, * = data
    //    H****H     We load a tile as in figure. This wastes bandwidth
    //    H****H     because halos are loaded two times each, but it is
    //    H****H     the easiest way to remain generic w.r.t. grid size
    //     HHHH

#ifdef USE_SHARED_PREV
    __shared__ T s_prev[WAVE_8_KER_ROWS][WAVE_8_KER_COLS];
#endif
    __shared__ T s_curr[WAVE_8_KER_ROWS+2*WAVE_8_HALO_SIZE][WAVE_8_KER_COLS+2*WAVE_8_HALO_SIZE];

    int maxrow  = params.maxrow;
    int maxcol  = params.maxcol;
    T   dt      = params.dt;
    T   c       = params.velocity;
    T   a       = params.damping;

    assert(maxcol > 1);
    assert(maxrow > 1);

    T c2dt2 = c*c*dt*dt;
    T kx2 = c2dt2 * (maxcol-1)*(maxcol-1);
    T ky2 = c2dt2 * (maxrow-1)*(maxrow-1);
    /* Don't ask me why but if you put the rhs of this in the computation
     * of val the kernel runs 2x slower */
    T one_minus_adt = (1.0 - a*dt);
    T two_minus_adt = (2.0 - a*dt);

    /* Get X/Y coordinates of this grid point */
    int ofs_x = blockDim.x * blockIdx.x + threadIdx.x + WAVE_8_HALO_SIZE;
    int ofs_y = blockDim.y * blockIdx.y + threadIdx.y + WAVE_8_HALO_SIZE;

    /* Get offset of this grid point in the fd grid array */
    int ofs = (maxcol + 2*WAVE_8_HALO_SIZE) * ofs_y + ofs_x;

    if ( (ofs_x < (maxcol + WAVE_8_HALO_SIZE)) and (ofs_y < (maxrow + WAVE_8_HALO_SIZE)) )
    {
        int i = threadIdx.y + WAVE_8_HALO_SIZE;
        int j = threadIdx.x + WAVE_8_HALO_SIZE;

        s_curr[i][j] = u_curr[ofs];

#ifdef USE_SHARED_PREV
        s_prev[threadIdx.y][threadIdx.x] = u_prev[ofs];
#endif
        if (threadIdx.x < WAVE_8_HALO_SIZE)
        {
            /* X-halo */
            int ox = min(WAVE_8_KER_COLS, maxcol-blockDim.x * blockIdx.x);
            s_curr[i][j-WAVE_8_HALO_SIZE] = u_curr[ofs-WAVE_8_HALO_SIZE];
            s_curr[i][j+ox] = u_curr[ofs+ox];
        }

        if (threadIdx.y < WAVE_8_HALO_SIZE)
        {
            /* Y-halo */
            int oy = min(WAVE_8_KER_ROWS, maxrow-blockDim.y * blockIdx.y);
            int b_ofs = ofs - WAVE_8_HALO_SIZE * (2*WAVE_8_HALO_SIZE+maxcol);
            int t_ofs = ofs + oy * (2*WAVE_8_HALO_SIZE+maxcol);
            s_curr[i-WAVE_8_HALO_SIZE][j]  = u_curr[b_ofs];
            s_curr[i+oy][j]  = u_curr[t_ofs];
        }

        __syncthreads();

        T* w;
        if ( std::is_same<T,float>::value)
            w = (T*)d_w32;
        if ( std::is_same<T,double>::value)
            w = (T*)d_w64;

        T deriv_x = 0.0;
        //deriv_x = kx2 * (s_curr[i][j-1] - 2.0*s_curr[i][j] + s_curr[i][j+1]);
        for (int k = -WAVE_8_HALO_SIZE; k <= WAVE_8_HALO_SIZE; k++)
            deriv_x += kx2 * w[k+WAVE_8_HALO_SIZE] * s_curr[i][j+k];

        T deriv_y = 0.0;
        //deriv_y = ky2 * (s_curr[i-1][j] - 2.0*s_curr[i][j] + s_curr[i+1][j]);
        for (int k = -WAVE_8_HALO_SIZE; k <= WAVE_8_HALO_SIZE; k++)
            deriv_y += ky2 * w[k+WAVE_8_HALO_SIZE] * s_curr[i+k][j];

#ifdef USE_SHARED_PREV
        T val = (deriv_x + deriv_y)
              - one_minus_adt * s_prev[threadIdx.y][threadIdx.x]
              + two_minus_adt * s_curr[i][j];
#else 
        T val = (deriv_x + deriv_y)
              - one_minus_adt * u_prev[ofs]
              + two_minus_adt * s_curr[i][j];
#endif

        if ( (ofs_x == WAVE_8_HALO_SIZE) or (ofs_y == WAVE_8_HALO_SIZE) or
             (ofs_x == maxcol+WAVE_8_HALO_SIZE-1) or (ofs_y == maxrow+WAVE_8_HALO_SIZE-1)
           )
            val = 0;

        u_next[ofs] = val;
    }
}

template<typename T>
double solve_cuda_aux(wave_equation_context<T>& wec)
{
    /* Get device properties */
    cudaDeviceProp prop;
    checkCuda( cudaGetDeviceProperties(&prop, 0) );
    printf("Device: %s (Compute Capability: %d.%d)\n",
        prop.name, prop.major, prop.minor);

    /**** Initialize constants ****/
    T w0 = -205.0/72.0;
    T w1 = 8.0/5.0;
    T w2 = -1.0/5.0;
    T w3 = 8.0/315.0;
    T w4 = -1.0/560.0;
    T w[9] = { w4, w3, w2, w1, w0, w1, w2, w3, w4 };

    if (std::is_same<T,float>::value)
        checkCuda( cudaMemcpyToSymbol(d_w32, w, 9*sizeof(T), 0, cudaMemcpyHostToDevice) );
    if (std::is_same<T,double>::value)
        checkCuda( cudaMemcpyToSymbol(d_w64, w, 9*sizeof(T), 0, cudaMemcpyHostToDevice) );

    /* Allocate memory on device */
    T* u_prev;
    T* u_curr;
    T* u_next;
    T* u_temp;

    checkCuda( cudaMalloc((void**)&u_prev, wec.g_prev.size()*sizeof(T)) );
    checkCuda( cudaMalloc((void**)&u_curr, wec.g_curr.size()*sizeof(T)) );
    checkCuda( cudaMalloc((void**)&u_next, wec.g_next.size()*sizeof(T)) );

    /* Copy grids on device */
    checkCuda( cudaMemcpy(u_prev, wec.g_prev.data(), wec.g_prev.size()*sizeof(T), cudaMemcpyHostToDevice) );
    checkCuda( cudaMemcpy(u_curr, wec.g_curr.data(), wec.g_curr.size()*sizeof(T), cudaMemcpyHostToDevice) );
    checkCuda( cudaMemcpy(u_next, wec.g_next.data(), wec.g_next.size()*sizeof(T), cudaMemcpyHostToDevice) );

    /* Launch parameters */
    dim3 grid_size(wec.g_curr.domain_cols()/WAVE_8_KER_COLS + 1, wec.g_curr.domain_rows()/WAVE_8_KER_ROWS + 1);
    dim3 threads_per_block(WAVE_8_KER_COLS, WAVE_8_KER_ROWS);

    /* Simulation parameters */
    wave_2D_params<T> params;
    params.maxcol = wec.g_curr.domain_cols();
    params.maxrow = wec.g_curr.domain_rows();
    params.dt = wec.dt;
    params.velocity = wec.velocity;
    params.damping = wec.damping;

    /* Prepare for timing */
    cudaEvent_t startEvent, stopEvent;
    checkCuda( cudaEventCreate(&startEvent) );
    checkCuda( cudaEventCreate(&stopEvent) );

    checkCuda( cudaEventRecord(startEvent, 0) );

    for (size_t i = 0; i < wec.maxiter; i++)
    {
        //sum_kernel_cuda<<<grid_size, threads_per_block>>>(u_prev, u_curr, u_next, params);
        wave_2D_kernel_cuda<<<grid_size, threads_per_block>>>(u_prev, u_curr, u_next, params);
        

        u_temp = u_prev;
        u_prev = u_curr;
        u_curr = u_next;
        u_next = u_temp;

#ifdef HAVE_SILO
#ifdef SAVE_TIMESTEPS
        if ( (i%100) == 0 )
        {
            checkCuda( cudaMemcpy(wec.g_curr.data(), u_curr, wec.g_curr.size()*sizeof(T), cudaMemcpyDeviceToHost) );
            std::stringstream ss;
            ss << "wave_cuda_" << i << ".silo";
            visit_dump(wec.g_curr, ss.str());
        }
#endif /* SAVE_TIMESTEPS */
#endif /* HAVE_SILO */
    }

    float milliseconds;
    checkCuda( cudaEventRecord(stopEvent, 0) );
    checkCuda( cudaEventSynchronize(stopEvent) );
    checkCuda( cudaEventElapsedTime(&milliseconds, startEvent, stopEvent) );
    

    std::cout << "[Wave][Cuda] Iteration Time: " << milliseconds/wec.maxiter << "ms" << std::endl;
    std::cout << "[Wave][Cuda] Wall Time: " << milliseconds << "ms" << std::endl;
    
    double itertime = milliseconds/wec.maxiter;

    double gflops_s = 70.0*(params.maxrow*params.maxcol)/(1e6*itertime);
    std::cout << "[Wave][Cuda] GFlops/s: " << gflops_s << std::endl;

    size_t kb1 = (2*WAVE_8_HALO_SIZE+WAVE_8_KER_ROWS)*(2*WAVE_8_HALO_SIZE+WAVE_8_KER_COLS);
    size_t kb2 = WAVE_8_KER_ROWS*WAVE_8_KER_COLS;
    size_t kernel_bytes = (kb1+2*kb2)*sizeof(T)*grid_size.x*grid_size.y;
    double gbytes_s = kernel_bytes/(1e6*itertime);
    std::cout << "[Wave][Cuda] Bandwidth: " << gbytes_s << "GB/s" << std::endl;

#ifdef HAVE_SILO
    checkCuda( cudaMemcpy(wec.g_curr.data(), u_curr, wec.g_curr.size()*sizeof(T), cudaMemcpyDeviceToHost) );
    visit_dump(wec.g_curr, "wave_cuda_lastiter.silo");
#endif /* HAVE_SILO */

    return itertime;
}

double solve_cuda(wave_equation_context<float>& wec)
{
    return solve_cuda_aux(wec);
}

double solve_cuda(wave_equation_context<double>& wec)
{
    return solve_cuda_aux(wec);
}






