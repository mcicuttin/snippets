cmake_minimum_required(VERSION 3.11 FATAL_ERROR)
project(fd_catalog)
include(CheckLanguage)
include(FetchContent)

set (CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

######################################################################
if (CMAKE_CXX_COMPILER_ID MATCHES "Clang")
    set(COMPILER_IS_CLANG TRUE)
elseif (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    set(COMPILER_IS_GNU TRUE)
elseif (CMAKE_CXX_COMPILER_ID STREQUAL "Intel")
    set(COMPILER_IS_INTEL)
elseif (CMAKE_CXX_COMPILER_ID STREQUAL "PGI")
    set(COMPILER_IS_PGI)
endif ()


set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)
if (COMPILER_IS_CLANG OR COMPILER_IS_INTEL OR COMPILER_IS_GNU)
    set(LINK_LIBS ${LINK_LIBS} Threads::Threads)
endif()

find_package(SILO)
if (SILO_FOUND)
    add_definitions(-DHAVE_SILO)
    include_directories("${SILO_INCLUDE_DIR}")
    set(LINK_LIBS ${LINK_LIBS} ${SILO_LIBRARY})
endif()

find_package(OpenACC)
if (OpenACC_CXX_FOUND)
    set(OPENACC_LINK_LIBS ${LINK_LIBS})
    if (COMPILER_IS_GNU)
        set(OPENACC_LINK_LIBS ${OPENACC_LINK_LIBS} -lgomp)
    endif()

    if (ENABLE_SINGLE)
        add_executable(fd_openacc_single fd_openacc.cpp)
        target_compile_definitions(fd_openacc_single PUBLIC -DSINGLE_PRECISION)
        target_compile_options(fd_openacc_single PUBLIC ${OpenACC_CXX_FLAGS})
        target_link_options(fd_openacc_single PUBLIC ${OpenACC_CXX_FLAGS})
        target_link_libraries(fd_openacc_single ${OPENACC_LINK_LIBS})
    endif()

    if (ENABLE_DOUBLE)
        add_executable(fd_openacc_double fd_openacc.cpp)
        target_compile_options(fd_openacc_double PUBLIC ${OpenACC_CXX_FLAGS})
        target_link_options(fd_openacc_double PUBLIC ${OpenACC_CXX_FLAGS})
        target_link_libraries(fd_openacc_double ${OPENACC_LINK_LIBS})
    endif()
endif()

option(ENABLE_TIMESTEP_OUTPUT "Save timesteps (don't use during perf meas)" OFF)
if (ENABLE_TIMESTEP_OUTPUT)
    if (NOT SILO_FOUND)
        message(FATAL_ERROR "You need SILO to output iteration data")
    endif()
    add_definitions(-DSAVE_TIMESTEPS)
endif()

option(ENABLE_ITERTIME_OUTPUT "Save iteration times (don't use during perf meas)" OFF)
if (ENABLE_ITERTIME_OUTPUT)
    add_definitions(-DSAVE_ITERTIME)
endif()

######################################################################
## Optimization stuff

option(OPT_PREFER_512bit "Prefer 512 bit vectors with AVX512 (Clang > 10 & GCC)" OFF)
if (OPT_PREFER_512bit)
    # https://reviews.llvm.org/D67259
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mprefer-vector-width=512")
endif()

option(OPT_AGGRESSIVE_FP "Enable DAZ, FTZ and -ffast-math" ON)
if (OPT_AGGRESSIVE_FP)
    add_definitions(-DDISALLOW_DENORMALS)
    if (COMPILER_IS_CLANG OR COMPILER_IS_GNU)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -ffast-math")
    endif()
endif()

option(OPT_VECTORIZER_REMARKS "Enable vectorizer remarks" OFF)
if (OPT_VECTORIZER_REMARKS)
    if (CMAKE_CXX_COMPILER_ID STREQUAL "Clang" OR CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Rpass=loop-vectorize -Rpass-missed=loop-vectorize -Rpass-analysis=loop-vectorize")
    endif()

    if (CMAKE_CXX_COMPILER_ID STREQUAL "Intel")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -qopt-report-phase=vec -qopt-report=2")
    endif()

    if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fopt-info-vec-optimized")
    endif()

    if (CMAKE_CXX_COMPILER_ID STREQUAL "PGI")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Minfo")
    endif()
endif()

######################################################################

option(ENABLE_KOKKOS "Enable Kokkos" OFF)
if (ENABLE_KOKKOS)
    FetchContent_Declare(kokkos
        GIT_REPOSITORY https://github.com/kokkos/kokkos.git
    )
    FetchContent_MakeAvailable(kokkos)

    if (ENABLE_SINGLE)
        add_executable(fd_kokkos_single fd_kokkos.cpp)
        target_compile_definitions(fd_kokkos_single PUBLIC -DSINGLE_PRECISION)
        target_link_libraries(fd_kokkos_single ${LINK_LIBS} Kokkos::kokkos)
    endif()

    if (ENABLE_DOUBLE)
        add_executable(fd_kokkos_double fd_kokkos.cpp)
        target_link_libraries(fd_kokkos_double ${LINK_LIBS} Kokkos::kokkos)
    endif()

    set(HAVE_KOKKOS TRUE)
endif()

option(ENABLE_CUDA "Enable CUDA if present" ON)
if (ENABLE_CUDA)
    check_language(CUDA)
    if (CMAKE_CUDA_COMPILER)
        enable_language(CUDA)
        set(CMAKE_CUDA_STANDARD 14)
        set(CMAKE_CUDA_STANDARD_REQUIRED ON)
        add_definitions(-DHAVE_CUDA)
        set(HAVE_CUDA TRUE)
    endif()
endif()

option(ENABLE_OPENMP "Enable OpenMP" OFF)
if (ENABLE_OPENMP)
    if (CMAKE_CXX_COMPILER_ID STREQUAL "Clang" OR CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fopenmp")
    endif()

    if (CMAKE_CXX_COMPILER_ID STREQUAL "Intel")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -qopenmp")
    endif()

    if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fopenmp")
    endif()
endif()

set(CMAKE_CXX_FLAGS_DEBUG "-g")
set(CMAKE_CXX_FLAGS_RELEASE "-O3 -g -DNDEBUG")
set(CMAKE_CXX_FLAGS_RELEASEASSERT "-O3 -g -fpermissive")

if (COMPILER_IS_CLANG OR COMPILER_IS_GNU OR COMPILER_IS_INTEL)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -march=native")
endif()
 

macro(setup_fd_catalog_target FD_TGT_NAME SINGLE_PRECISION)
    set(FD_SOURCES "fd_main.cpp")

    if (HAVE_CUDA)
        set(FD_SOURCES ${FD_SOURCES} fd_wave_cuda.cu)
    endif()

    add_executable(${FD_TGT_NAME} ${FD_SOURCES})

    if (HAVE_CUDA AND APPLE)
        set_property(TARGET ${FD_TGT_NAME} PROPERTY BUILD_RPATH /usr/local/cuda/lib)
    endif()
    
    if (${SINGLE_PRECISION})
        target_compile_definitions(${FD_TGT_NAME} PUBLIC -DSINGLE_PRECISION)
    endif()

    target_link_libraries(${FD_TGT_NAME} ${LINK_LIBS})
endmacro()

option(ENABLE_SINGLE "Enable single precision build" ON)
if (ENABLE_SINGLE)
    setup_fd_catalog_target("fd_catalog_single" TRUE)
endif()

option(ENABLE_DOUBLE "Enable double precision build" ON)
if (ENABLE_DOUBLE)
    setup_fd_catalog_target("fd_catalog_double" FALSE)
endif()


