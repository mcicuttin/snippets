#include <iostream>
#include <fstream>
#include <cstdio>
#include <unistd.h>

#include <pmmintrin.h>
#include <xmmintrin.h>

#include "fd_wave_cpu.hpp"

#ifdef HAVE_CUDA
#include "fd_wave_cuda.hpp"
#endif

int main(int argc, char **argv)
{
#ifdef SINGLE_PRECISION
    using T = float;
    std::cout << "Precision: single" << std::endl;
    std::ofstream ofs("timings-float.txt");
#else
    using T = double;
    std::cout << "Precision: double" << std::endl;
    std::ofstream ofs("timings-double.txt");
#endif

#ifdef DISALLOW_DENORMALS
    _MM_SET_FLUSH_ZERO_MODE(_MM_FLUSH_ZERO_ON);
    _MM_SET_DENORMALS_ZERO_MODE(_MM_DENORMALS_ZERO_ON);
    std::cout << "Denormals: FTZ and DAZ" << std::endl;
#endif

    _MM_SET_EXCEPTION_MASK(_MM_GET_EXCEPTION_MASK() & ~_MM_MASK_INVALID);
    //_MM_SET_EXCEPTION_MASK(_MM_GET_EXCEPTION_MASK() & ~_MM_MASK_DENORM);

    /* Make header */
    ofs << "\"SIZE\"    \"Seq\"    \"SeqBlk\"    ";

#ifdef HAVE_CUDA
    ofs << "\"Cuda\"    ";
#endif

    auto maxthreads = std::thread::hardware_concurrency();
    for (size_t threads = 1; threads < maxthreads; threads *= 2)
        ofs << "\"" << threads << " threads\"    ";
    ofs << std::endl;

    double time;

    for (size_t sz = 128; sz <= 1024; sz *= 2)
    {
        wave_equation_context<T> wec(sz, sz, 1, 0.1, 0.0001, 5000);
        ofs << sz << "    ";

        wec.init();
        time = solve_sequential(wec);
        ofs << time << "    ";
        
#ifndef _OPENMP
        wec.init();
        time = solve_sequential_blocked(wec);
        ofs << time << "    ";

#ifdef HAVE_CUDA
        wec.init();
        time = solve_cuda(wec);
        ofs << time << "    ";
#endif
        for (size_t threads = 1; threads < maxthreads; threads *= 2)
        {
            wec.init();
            time = solve_multithread(wec, threads);
            ofs << time << "    ";
        }
#endif /* _OPENMP */

        ofs << std::endl;
    }
}



