/* Performance evaluation of different implementations of finite difference
 * schemes.
 *
 * Matteo Cicuttin (C) 2020, Université de Liège, ACE Group
 */

#pragma once

#include <vector>
#include <cstdint>

template<typename T, typename UInt = uint64_t, typename Int = int64_t>
class fd_grid
{
    std::vector<T>  m_data;
    UInt    m_grid_rows, m_grid_cols;
    UInt    m_domain_rows, m_domain_cols, m_halo;

public:
    fd_grid()
    {}

    fd_grid(UInt rows, UInt cols, UInt halo) :
        m_grid_rows(rows+2*halo),
        m_grid_cols(cols+2*halo),
        m_domain_rows(rows),
        m_domain_cols(cols),
        m_halo(halo),
        m_data( (rows+2*halo) * (cols+2*halo) )
    {}

    fd_grid(fd_grid&& other)
    {
        m_grid_rows = other.m_grid_rows;
        m_grid_cols = other.m_grid_cols;
        m_domain_rows = other.m_domain_rows;
        m_domain_cols = other.m_domain_cols;
        m_halo = other.m_halo;
        m_data = std::move(other.m_data);
    }

    fd_grid& operator=(const fd_grid& other)
    {
        m_grid_rows = other.m_grid_rows;
        m_grid_cols = other.m_grid_cols;
        m_domain_rows = other.m_domain_rows;
        m_domain_cols = other.m_domain_cols;
        m_halo = other.m_halo;
        m_data.resize( other.m_data.size() );
        std::copy(other.m_data.begin(), other.m_data.end(), m_data.begin());
        return *this;
    }

    fd_grid& operator=(fd_grid&& other)
    {
        m_grid_rows = other.m_grid_rows;
        m_grid_cols = other.m_grid_cols;
        m_domain_rows = other.m_domain_rows;
        m_domain_cols = other.m_domain_cols;
        m_halo = other.m_halo;
        m_data = std::move(other.m_data);
        return *this;
    }

    fd_grid& operator=(const T& val)
    {
        for (auto& d : m_data)
            d = val;
        return *this;
    }

    /* Negative indices are allowed to access halo */
    T& operator()(Int i, Int j)
    {
        auto ofs_row        = i + m_halo;
        assert(ofs_row < m_domain_rows + 2*m_halo);

        auto ofs_col        = j + m_halo;
        assert(ofs_col < m_domain_cols + 2*m_halo);

        auto grid_cols      = m_domain_cols+2*m_halo;
        auto ofs            = ofs_row * grid_cols + ofs_col;
        assert( (ofs >= 0) and (ofs < m_data.size()) );
        return m_data[ofs];
    }

    /* Negative indices are allowed to access halo */
    T operator()(Int i, Int j) const
    {
        auto ofs_row        = i + m_halo;
        auto ofs_col        = j + m_halo;
        auto grid_cols      = m_domain_cols+2*m_halo;
        auto ofs            = ofs_row * grid_cols + ofs_col;
        assert( (ofs >= 0) and (ofs < m_data.size()) );
        return m_data[ofs];
    }

    size_t domain_rows() const { return m_domain_rows; }
    size_t domain_cols() const { return m_domain_cols; }
    size_t halo()        const { return m_halo; }

    size_t grid_rows()   const { return m_grid_rows; }
    size_t grid_cols()   const { return m_grid_cols; }

    T*          data()          { return m_data.data(); }
    const T*    data() const    { return m_data.data(); }

    T* data(Int i, Int j)
    {
        auto ofs_row        = i + m_halo;
        assert(ofs_row < m_domain_rows + 2*m_halo);

        auto ofs_col        = j + m_halo;
        assert(ofs_col < m_domain_cols + 2*m_halo);

        auto grid_cols      = m_domain_cols+2*m_halo;
        auto ofs            = ofs_row * grid_cols + ofs_col;
        assert( (ofs >= 0) and (ofs < m_data.size()) );
        return m_data.data() + ofs;
    }

    const T* data(Int i, Int j) const
    {
        auto ofs_row        = i + m_halo;
        assert(ofs_row < m_domain_rows + 2*m_halo);

        auto ofs_col        = j + m_halo;
        assert(ofs_col < m_domain_cols + 2*m_halo);

        auto grid_cols      = m_domain_cols+2*m_halo;
        auto ofs            = ofs_row * grid_cols + ofs_col;
        assert( (ofs >= 0) and (ofs < m_data.size()) );
        return m_data.data() + ofs;
    }

    size_t      size() const    { return m_data.size(); }

    T           dx() const { return 1./(m_domain_cols-1); }
    T           dy() const { return 1./(m_domain_rows-1); }
};

