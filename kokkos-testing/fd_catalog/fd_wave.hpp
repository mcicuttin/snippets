/* Performance evaluation of different implementations of finite difference
 * schemes.
 *
 * Matteo Cicuttin (C) 2020, Université de Liège, ACE Group
 */

#pragma once

#define WAVE_8_HALO_SIZE           4

template<typename T>
struct wave_2D_params
{
    int maxrow;
    int maxcol;
    T   dt;
    T   velocity;
    T   damping;
};

template<typename T>
struct wave_equation_context
{
    fd_grid<T> g_prev;
    fd_grid<T> g_curr;
    fd_grid<T> g_next;

    T       velocity;
    T       damping;
    T       dt;
    int     maxiter;

    size_t  rows, cols;

    wave_equation_context(size_t prows, size_t pcols, T vel, T damp, T pdt, int pmaxiter)
        : rows(prows), cols(pcols)
    {
        g_prev = fd_grid<T>(rows, cols, WAVE_8_HALO_SIZE);
        g_curr = fd_grid<T>(rows, cols, WAVE_8_HALO_SIZE);
        g_next = fd_grid<T>(rows, cols, WAVE_8_HALO_SIZE);
        velocity = vel;
        damping = damp;
        dt = pdt;
        maxiter = pmaxiter;

        init();
    }

    void init(void)
    {
        for (size_t i = 0; i < g_curr.domain_rows(); i++)
        {
            for (size_t j = 0; j < g_curr.domain_cols(); j++)
            {
                T y = g_curr.dy()*i - 0.3;
                T x = g_curr.dx()*j - 0.1;
                g_prev(i,j) = -std::exp(-2400*(x*x + y*y));
                g_curr(i,j) = 2*dt*g_prev(i,j);
                g_next(i,j) = 0.0;
            }
        }
    }
};

