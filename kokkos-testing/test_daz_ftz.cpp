#include <iostream>
#include <pmmintrin.h>
#include <xmmintrin.h>

int main(void)
{
    std::cout << "DAZ: " << _MM_GET_DENORMALS_ZERO_MODE() << std::endl;
    std::cout << "FTZ: " << _MM_GET_FLUSH_ZERO_MODE() << std::endl;
    return 0; 
}
