project(kokkos-testing)
cmake_minimum_required(VERSION 3.10)

find_package(CUDA)

add_subdirectory(kokkos)

add_executable(finite_difference_kokkos finite_difference_kokkos.cpp)
target_link_libraries(finite_difference_kokkos Kokkos::kokkos siloh5)

add_executable(finite_difference_baseline finite_difference_baseline.cpp)
target_link_libraries(finite_difference_baseline siloh5)
 

