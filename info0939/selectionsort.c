#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

#define ARRAY_LEN 8
#define VALUE_MAX 100

static void
selectionsort(int *a, size_t N)
{
    assert(N > 0);
    for (size_t i = 0; i < N-1; i++)
    {
        size_t min_pos = i;
        for (size_t j = 0; j < N; j++)
            if (a[j] < a[min_pos])
                min_pos = j;

        if (min_pos != i)
        {
            int tmp = a[i];
            a[i] = a[min_pos];
            a[min_pos] = tmp;
        }
    }
}

static void
print_array(const int *a, size_t N)
{
    for (size_t i = 0; i < N; i++)
        printf("%d ", a[i]);
    printf("\n");
}

int main(void)
{
    srand(time(NULL));
    int a[ARRAY_LEN];
    for (size_t i = 0; i < ARRAY_LEN; i++)
        a[i] = rand() % VALUE_MAX;

    print_array(a, ARRAY_LEN);
    selectionsort(a, ARRAY_LEN);
    print_array(a, ARRAY_LEN);

    return 0;
}

