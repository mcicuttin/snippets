#include <stdio.h>
#include <sys/resource.h>
#include <cblas.h>
#include <immintrin.h>
#include <stdlib.h>

#define VEC_SIZE 320000000ULL
#define BLOCK_SIZE 8

static void *
Alloc_doubles(size_t size)
{
    void *ret = aligned_alloc(32, size*sizeof(double));
    if (!ret)
    {
        printf("malloc() failed\n");
        abort();
    }
    return ret;
}

static double
compute_time(const struct rusage *rstart, const struct rusage *rend)
{
    double ret = rend->ru_utime.tv_sec + rend->ru_utime.tv_usec/1e6;
    ret -= rstart->ru_utime.tv_sec + rstart->ru_utime.tv_usec/1e6;
    return ret;
}

static double
scal(const double * __restrict__ a, const double * __restrict__ b, size_t len)
{
    double ret = 0.0;
    size_t blocks = len/BLOCK_SIZE;

    for (size_t blk = 0; blk < blocks; blk++)
        for (size_t j = 0; j < BLOCK_SIZE; j++)
            ret += a[blk*BLOCK_SIZE+j]*b[blk*BLOCK_SIZE+j];

    for (size_t i = blocks*BLOCK_SIZE; i < len; i++)
        ret += a[i]*b[i];

    return ret;
}

static double
scal_avx(const double * __restrict__ a, const double * __restrict__ b, size_t len)
{
    double ret = 0.0;
    size_t blocks = len/4;
    for (size_t blk = 0; blk < blocks; blk++)
    {
        __m256d av      = _mm256_load_pd(a+blk*4);
        __m256d bv      = _mm256_load_pd(b+blk*4);
        __m256d mv      = _mm256_mul_pd(av, bv);
        __m128d lowv    = _mm256_castpd256_pd128(mv);
        __m128d highv   = _mm256_extractf128_pd(mv, 1);
                lowv    = _mm_add_pd(lowv, highv);
        __m128d high64  = _mm_unpackhi_pd(lowv, lowv);
        ret +=  _mm_cvtsd_f64(_mm_add_sd(lowv, high64));
    }

    for (size_t i = blocks*4; i < len; i++)
        ret += a[i]*b[i];

    return ret;
}

int main(void)
{
    double *a = (double *) Alloc_doubles(VEC_SIZE);
    double *b = (double *) Alloc_doubles(VEC_SIZE);

    for (size_t i = 0; i < VEC_SIZE; i++)
    {
        a[i] = ((double) i)/VEC_SIZE;
        b[i] = ((double) i)/VEC_SIZE;
    }

    struct rusage rstart, rend;
    getrusage(RUSAGE_SELF, &rstart);
    double s = scal(a, b, VEC_SIZE);
    //double s = scal_avx(a, b, VEC_SIZE);
    //double s = cblas_ddot(VEC_SIZE, a, 1, b, 1);
    getrusage(RUSAGE_SELF, &rend);

    printf("%lg\n", s);

    double time = compute_time(&rstart, &rend);
    double gflops = 2*VEC_SIZE/(time*1e9);
    double gbs = 2*VEC_SIZE*sizeof(double)/(time*1e9);

    printf("GFLOPS/s: %lg\n", gflops);
    //printf("GB/s: %lg\n", gbs);

    return 0;
}

