#include <stdio.h>
#include <stdint.h>

static uint64_t
fib(uint32_t n)
{
    if (n < 2)
        return 1;

    return fib(n-1) + fib(n-2);
}

static uint64_t
fact(uint32_t n)
{
    if (n > 0)
        return n*fact(n-1);

    return 1;
}

int main(void)
{
    printf("%llu\n", fib(42));
    printf("%llu\n", fact(42));
    return 0;
}
