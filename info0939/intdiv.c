#include <stdio.h>
#include <assert.h>

typedef struct div_result {
    int quo;
    int rem;
} div_result_t;

static void
integer_division(int x, int y, struct div_result *res)
{
    /* Precondition: for the program to work, the input must satisfy
     * x >= 0 (just to keep things simple) 
     * y > 0 (because division by zero is not defined)
     */
    assert(x >= 0 && y > 0);

    res->quo = 0;
    res->rem = x;

    /* The loop invariant is that
     * - quotient * y + res has to be equal to x
     * - y > 0
     * The invariant must be true before  and after the loop, and
     * at the end of its body.
     */
    assert( (res->quo*y + res->rem == x) && res->rem >= 0 && y > 0 );
    
    while (res->rem >= y)
    {
        /* At this point invariant AND while condition should be valid */
        assert( (res->quo*y + res->rem == x) && res->rem >= 0 && y > 0 && res->rem >= y );

        res->rem = res->rem - y;
        res->quo = res->quo + 1;

        /* At this point the invariant should be re-established */
        assert( (res->quo*y + res->rem == x) && res->rem >= 0 && y > 0 );
    }

    /* Postcondition: invariant AND NOT while condition */
    assert( (res->quo*y + res->rem == x) && res->rem >= 0 && y > 0 && res->rem < y );

    /* In addition, note that IF we enter the cycle THEN res.rem ALWAYS
     * decreases. res.rem is a ``bound function'' and allows you to say
     * that your program ALWAYS terminates. */
}

int main(void)
{
    struct div_result res;
    integer_division(10, 4, &res);
    printf("%d %d\n", res.quo, res.rem);
    return 0;
}
