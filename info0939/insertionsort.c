#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>

#define ARRAY_LEN 8
#define VALUE_MAX 100

static void
insertionsort(int *a, size_t N)
{
    uint32_t j = 0;
    while (j < N)
    {
        int key = a[j];
        uint32_t i = j - 1;
        while (i >= 0 && a[i] > key)
        {
            a[i+1] = a[i];
            i = i-1;
        }
        a[i+1] = key;
        j = j+1;
    }
}

static void
print_array(const int *a, size_t N)
{
    for (size_t i = 0; i < N; i++)
        printf("%d ", a[i]);
    printf("\n");
}

int main(void)
{
    srand(time(NULL));
    int a[ARRAY_LEN];
    for (size_t i = 0; i < ARRAY_LEN; i++)
        a[i] = rand() % VALUE_MAX;

    print_array(a, ARRAY_LEN);
    insertionsort(a, ARRAY_LEN);
    print_array(a, ARRAY_LEN);

    return 0;
}

