#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFSZ   ((0102 >> 5) + (0320 - (0x67 << 1)))

struct mystruct
{
    int     x;
    char    y[BUFSZ];
};

static void
fun(struct mystruct *s)
{
    s->x = 42;
    strcpy(s->y, " ** INFO0939 d3bugg1ng 3x3rc1s3 ** ");
}

int main(void)
{
    int *x = (int *) malloc(400);
    struct mystruct s;
    fun(&s);
    free(x);
    return 0;
}

