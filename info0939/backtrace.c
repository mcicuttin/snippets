#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

#define ARRAY_LEN 10
#define VALUE_MAX 100

static void
sum(int *r, const int *a, const int *b, size_t N)
{
    for (size_t i = 0; i < N; i++)
        r[i] = a[i] + b[i];
}

static void
f(int *r , const int *a, const int *b, size_t N)
{
    return sum(r,a,b,N);
}

static void
g(int *r, const int *a, const int *b, size_t N)
{
    return sum(r,a,b,N-200);
}

int main(void)
{
    srand(time(NULL));
    int *a = (int *) malloc(ARRAY_LEN * sizeof(int));
    int *b = (int *) malloc(ARRAY_LEN * sizeof(int));
    int *r = (int *) malloc(ARRAY_LEN * sizeof(int));

    if (!a || !b || !r)
    {
        printf("malloc() failed\n");
        return 1;
    }

    for (size_t i = 0; i < ARRAY_LEN; i++)
    {
        a[i] = rand() % VALUE_MAX;
        b[i] = rand() % VALUE_MAX;
    }

    f(r, a, b, ARRAY_LEN);
    g(r, a, b, ARRAY_LEN);

    free(a);
    free(b);
    free(r);

    return 0;
}

