Prerequisites for the class:
---
 * Having access to a Unix machine
 * Having access to the compiler (`gcc` or `clang`), the debugger (`gdb`), Valgrind and `wget`
 * NIC5 satisfies all the requirements

Instructions for downloading the example code
---
The git repository is located at [here](https://gitlab.onelab.info/mcicuttin/snippets/info0939) (`https://gitlab.onelab.info/mcicuttin/snippets/info0939`). We will use the following files (in that order):  
- `intdiv.c` for the `assert()` example on the integer division program   
- `selectionsort.c` first example of GDB usage (stepping, breakpointing & inspecting)  
- `backtrace.c` second example of GDB usage (backtrace)  
- `insertionsort.c` first debugging exercise  
- `stack_corrupt.c` second debugging exercise  
- `profiling.c` for a trivial example of use of `gprof`  
- `cache.c` for an exaple of use of `cachegrind`    
- `scalprod.c` profiling exercise  

You can download the files on your machine or on NIC5 via `wget`:

```
# This command is needed only when you open a new shell
export REPO_BASE=https://gitlab.onelab.info/mcicuttin/snippets/-/raw/master/info0939

# Files are then donwloaded as following:
wget $REPO_BASE/filename.c
```

GDB Cheatsheet
---
To debug with GDB: `gdb <progname>`, then  

* `r`: Run the program  
* `bt`: Print the backtrace of a crashed program  
* `frame`: Select the stack frame you want to analyze  
* `s` or `step`: step one statement, enter the function if statement is a function call   
* `n` or `next`: execute next statement
* `c`: continue until the end of the program or the next breakpoint
* `break <file:line>`: set a breakpoint
* `p`: print the value of a variable
* `x/ct <address>` examine the contents of memory, where `c` is the count and `t` is the type. `t` takes more or less the same values of the `printf()` format specifiers. Example: `x/8d array` will print 8 integers starting from the address contained in `array`

Example: Integer division & `assert()`
---

* Compile with `gcc -g -o intdiv intdiv.c` (debug mode)  
* Compile with `gcc -O3 -DNDEBUG -o intdiv intdiv.c` (release mode)  
* Try to pass a negative integer to the function, compile the program in both modes and see what happens

Debugging example `selectionsort.c`
---
Examples of stepping, breakpointing & inspecting values with GDB

* Compile with `gcc -g -o selectionsort selectionsort.c`

Debugging example `backtrace.c`
---
Example of using the `backtrace` command in GDB

* Compile with `gcc -g -o backtrace backtrace.c`


Debugging exercise: `insertionsort.c`
---

* Compile with `gcc -g -o insertionsort insertionsort.c`
* Run with `./insertionsort`. What do you see?
* Launch the code in GDB and find why it crashes

Debugging exercise: `stack_corrupt.c`
---

* Compile with `gcc -g -o stack_corrupt stack_corrupt.c`
* Run with `./stack_corrupt`. What do you see?
* Launch the code in GDB and try to explain what you see

Profiling example with `profiling.c`
---
Example usage of `gprof`

* Compile with `gcc -g -pg -o profiling profiling.c`
* Run with `gprof profiling`

Profiling example with `cache.c`
---
Example usage of cachegrind to find cache-unfriendly code

* Compile with `gcc -O3 -g -o cache cache.c`
* Run with `valgrind --tool=cachegrind ./cache`

Profiling exercise (guided) with `scalprod.c`
---

* [Benchmark](https://www.cpubenchmark.net/cpu.php?cpu=AMD+EPYC+7542&id=3604)  
* [Specs](https://en.wikichip.org/wiki/amd/epyc/7542)  