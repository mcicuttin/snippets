#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/resource.h>

static void *
Malloc(size_t size)
{
    void *ret = malloc(size);
    if (!ret)
    {
        printf("malloc() failed\n");
        abort();
    }
    return ret;
}

static void
preheat(double *out, const double *mat, const double *vec,
    size_t m, size_t n)
{
    for (size_t i = 0; i < m; i++)
        for (size_t j = 0; j < n; j++)
            out[i] += mat[i*n + j] * vec[j];
}

static void
mvp_good(double *out, const double *mat, const double *vec,
    size_t m, size_t n)
{
    for (size_t i = 0; i < m; i++)
        for (size_t j = 0; j < n; j++)
            out[i] += mat[i*n + j] * vec[j];
}

static void
mvp_bad(double *out, const double *mat, const double *vec,
    size_t m, size_t n)
{
    for (size_t j = 0; j < n; j++)
        for (size_t i = 0; i < m; i++)
            out[i] += mat[i*n + j] * vec[j];
}

static double
compute_time(const struct rusage *rstart, const struct rusage *rend)
{
    double ret = rend->ru_utime.tv_sec + rend->ru_utime.tv_usec/1e6;
    ret -= rstart->ru_utime.tv_sec + rstart->ru_utime.tv_usec/1e6;
    return ret;
}

int
main(int argc, char * const argv[])
{
    int rows = 0, cols = 0;
    int ch;

    while ( (ch = getopt(argc, argv, "r:c:")) != -1 )
    {
        switch (ch)
        {
            case 'r':
                rows = atoi(optarg);
                break;

            case 'c':
                cols = atoi(optarg);
                break;

            case '?':
            default:
                printf("Usage: %s -r <rows> -c <cols>\n", argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    if (rows < 1 || cols < 1)
    {
        printf("Number of rows and cols must be >1\n");
        exit(EXIT_FAILURE);
    }

    double *mat = (double *) Malloc(rows*cols*sizeof(double));
    double *vec = (double *) Malloc(cols*sizeof(double));
    double *out = (double *) Malloc(rows*sizeof(double));

    preheat(out, mat, vec, rows, cols);

    struct rusage rstart, rend;
    getrusage(RUSAGE_SELF, &rstart);
    mvp_good(out, mat, vec, rows, cols);
    getrusage(RUSAGE_SELF, &rend);

    double time = compute_time(&rstart, &rend);
    printf("Good MVP: %g secs\n", time);

    getrusage(RUSAGE_SELF, &rstart);
    mvp_bad(out, mat, vec, rows, cols);
    getrusage(RUSAGE_SELF, &rend);

    time = compute_time(&rstart, &rend);
    printf("Bad MVP: %g secs\n", time);

    free(out);
    free(vec);
    free(mat);
    return 0;
}

