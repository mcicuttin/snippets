#include <stdio.h>

int testfun(int);
int abc(int);
int def(int);

int main(void)
{
    printf("%d\n", testfun(42));
    printf("%d\n", abc(42));
    printf("%d\n", def(42));
    return 0;
}

